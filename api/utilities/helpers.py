from configparser import ConfigParser, Error


def read_credentials_from_file(self, file_name: str) -> dict:
    try:
        config = ConfigParser()
        return config.read(file_name)
    except Error:
        print('trying to read credentials from {} which does not exist.', file_name)
