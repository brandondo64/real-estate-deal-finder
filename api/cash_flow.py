from flask import request
from flask_api import FlaskAPI

app = FlaskAPI(__name__)


@app.route("/cash-flows", methods=['POST'])
def post_for_cash_flow_calculations():
    address = str(request.data.get('address', ''))
    calculations = 12312

    return {
        'address': address,
        'message': 'Hello from the API!'
    }


@app.errorhandler(404)
def not_found(e):
    return '', 404


if __name__ == "__main__":
    app.run(debug=True)